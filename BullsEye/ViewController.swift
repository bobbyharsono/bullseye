//
//  ViewController.swift
//  BullsEye
//
//  Created by Bobby Harsono on 06/01/19.
//  Copyright © 2019 Bobby Harsono. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // component
    @IBOutlet weak var slider:UISlider!;
    @IBOutlet weak var targetNumberLbl:UILabel!;
    @IBOutlet weak var scoreNumberLbl:UILabel!;
    @IBOutlet weak var roundNumberLbl:UILabel!;
    
    // variables
    var currentSliderValue = 0;
    var targetValue = 0;
    var score = 0;
    var round = 1;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        currentSliderValue = Int(slider.value.rounded());
        
        // initialize slider visual
        let thumbnailImgSlider = #imageLiteral(resourceName: "SliderThumb-Normal")
        let thumbnailHighlightSlider = #imageLiteral(resourceName: "SliderThumb-Highlighted")
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        let leftTrackSlider = #imageLiteral(resourceName: "SliderTrackLeft")
        let leftTrackSliderResizeable = leftTrackSlider.resizableImage(withCapInsets: insets)
        let rightTrackSlider = #imageLiteral(resourceName: "SliderTrackRight")
        let rightTrackSliderResizeable = rightTrackSlider.resizableImage(withCapInsets: insets)
        
        
        slider.setThumbImage(thumbnailImgSlider, for: .normal)
        slider.setThumbImage(thumbnailHighlightSlider, for: .highlighted)
        slider.setMinimumTrackImage(leftTrackSliderResizeable, for: .normal)
        slider.setMaximumTrackImage(rightTrackSliderResizeable, for: .normal)
        
        
        newRound();
    }

    @IBAction func showAlert() {
        // increment score
        let difference = abs(targetValue - currentSliderValue);
        var points = Int(slider.maximumValue) - difference;
        
        // message to be shown
        let title:String;
        if(difference==0) {
            points += 100;
            title = "Perfect!!!";
        } else if(difference<5){
            title = "Almost there!!!";
            if (difference==1){
                points += 50;
            }
        } else if(difference<10){
            title = "Pretty good...";
        } else {
            title = "Try again...";
        }
        
        // increment score
        score += points;
        
        // increment round
        round += 1;
        
        
        let message = "Your point is \(points)";
        
        // create an alert controller
        let alert = UIAlertController(title: title, message: message, preferredStyle:.alert)
        
        // what action for the alert controller
        let action = UIAlertAction(title: "Gotcha!!!", style: .default, handler: {
            action in
            self.reset();
        })
        
        // link alert and action
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func getValueFromSlider(_ slider:UISlider){
        currentSliderValue = Int(slider.value.rounded())
    }
    
    @IBAction func startNewRound(_ newRoundBtn:UIButton){
        newRound();
    }
    
    func newRound(){
        reset();
        score = 0;
        scoreNumberLbl.text = String(score);
        round = 1;
        roundNumberLbl.text = String(round);
    }
    
    func reset(){
        targetValue = Int.random(in: 1...100);
        targetNumberLbl.text = String(targetValue);
        currentSliderValue = 50;
        slider.value = Float(currentSliderValue);
        scoreNumberLbl.text = String(score);
        roundNumberLbl.text = String(round);
    }
    
    
    
    
}

