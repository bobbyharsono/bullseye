//
//  AboutViewController.swift
//  BullsEye
//
//  Created by Bobby Harsono on 03/09/19.
//  Copyright © 2019 Bobby Harsono. All rights reserved.
//

import UIKit
import WebKit

class AboutViewController: UIViewController {
    
    @IBOutlet weak var aboutWebView:WKWebView!;

    override func viewDidLoad() {
        super.viewDidLoad()

        // get local path for our needed image and load it
        if let htmlPath = Bundle.main.path(forResource: "BullsEye", ofType: "html"){
            let urlPath = URL.init(fileURLWithPath: htmlPath)
            let request = URLRequest(url: urlPath, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 5)
            aboutWebView.load(request)
        }
    }
    

    @IBAction func close(){
        dismiss(animated: true, completion: nil)
    }

}
